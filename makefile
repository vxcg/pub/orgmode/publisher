SHELL:=/bin/bash

# fully qualified name of the prj_directory
# argument to this makefile's configure target
# prj_dir must have src and build directories
prj_dir=UNINITIALIZED

# config file relative to prj_dir
# argument to this makefile's configure target
config_file=${prj_dir}/config.py


# directory at which all related resources are installed.  This must
# match the ext variable in config_wf.py
ext=/tmp/reuse

bash_utils=bash-utils
bash_utils_dir=${ext}/${bash_utils}
bash_utils_repo=https://github.com/vlead/bash-utils.git

# directory where python 2 utilities are installed must match
# directory mentioned in init.sh
py2_utils=py2_utils
py2_utils_dir=${ext}/${py2_utils}
py2_utils_repo=https://github.com/vlead/py2_utils.git


init:
	(source ./init.sh;  mkdir -p ${ext}; git-install ${bash_utils_dir} ${bash_utils_repo})

py2: init
	(source ./init.sh; git-install ${py2_utils_dir} ${py2_utils_repo})

configure: py2
	(export PYTHONPATH=:${py2_utils_dir}:;  python -m config_wf ${prj_dir} ${config_file} ${ext})

clean-all:  clean
	(rm -rf ${ext}; rm -rf ${prj_dir}/exp; rm -rf ${prj_dir}/src/org-templates ${prj_dir}/src/style ${prj_dir}/src/math-templates ${prj_dir}/src/math-style)

clean:
	(rm -rf *.pyc)

test:
		(export PYTHONPATH=:${py2_utils_dir}:; python -m unittest discover)


