# Check if emacs version is compatible
def  check_emacs_exporter_compatible(catalog, resource_type, \
                                     resource, exporter):
    if resource in catalog[resource_type][exporter]:
        return True
    raise Exception("your emacs version %s is incompatible with exporter %s" \
                    % (emacs_ver, exporter))
