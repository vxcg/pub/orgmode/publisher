# -*- coding: utf-8 -*-
# Author:  Venkatesh Choppella <venkatesh.choppella@iiit.ac.in>
# Licence: GPL v3.0

# Workflow  for configuring Publisher
# -----------------------------------

# This script is to be  executed from the home directory of the
# publisher.

# Usage:
# ------
# (export PYTHONPATH=:py2_utils: ; python -m config_wf <prj_dir> <config_file>)

import sys
import os.path


from wf.jobs.git_clone_job import *
from wf.jobs.git_pull_job import *
from wf.jobs.git_install_job import *
from wf.jobs.symlink_job import *
from wf.jobs.targz_install_job import *

from wf.funs.git_clone_fun import *
from wf.funs.git_pull_fun import *
from wf.funs.git_install_fun import *
from wf.funs.symlink_fun import *
from wf.funs.targz_install_fun import *

from sh_utils.task_funs import *
from publish_funs import *

# =================
# Dependency chain
# =================

# Tasks in a set are independent
# of each other:

# [catalog, config] -> [emacs] -> [exporter, theme, org] ->
# -> [elisp, org-templates, style]

# project directory
# by default is the parent directory
# Overriden by first argument to this program

def main(argv):
    prj_dir=argv[0]
    config_file=argv[1]
    ext=argv[2]
    config_path=os.path.join(prj_dir, config_file)

    # src directory is fixed relative to prj_dir
    src_dir=os.path.join(prj_dir, "src")
    print "src_dir = %s" % src_dir
    
    # build directory is fixed relative to prj_dir
    build_dir=os.path.join(prj_dir, "build")

    exporters_dir = os.path.join(ext, "exporters")
    org_distros_dir = os.path.join(ext, "org-distros")
    os.system('mkdir -p %s' % exporters_dir)
    os.system('mkdir -p %s' % org_distros_dir)    


    # read config file
    config=import_file(config_path)
    emacs=config.__dict__.get('emacs', 'emacs')
    exporter=config.__dict__.get('exporter', 'org-v8')
    theme=config.__dict__.get('theme', 'readtheorg')
    math_theme=config.__dict__.get('math_theme', None)
    http_proxy=config.__dict__.get('http_proxy', '')
    https_proxy=config.__dict__.get('https_proxy', '')

    print "http_proxy = %s " % http_proxy
    print "https_proxy = %s " % https_proxy    

    

    # specify catalog location
    cat_repo="https://gitlab.com/vxcg/pub/orgmode/org-config-catalog.git"
    cat_ws_name='org-config-catalog'
    cat_file="catalog.py"

    # install catalog
    cat_job = GitInstallJob(wd=ext, repo=cat_repo)
    cat_res = GitInstallFun.do(cat_job)
    cat_path = os.path.join(ext, cat_ws_name, cat_file)
    cat_mod = import_file(cat_path)
    catalog = cat_mod.the_catalog


    # check if emacs_ver compatible with exporter
    emacs_version=emacs_ver(emacs)
    check_emacs_exporter_compatible(catalog, "emacsen", emacs_version, exporter)


    # install org
    org_url = catalog["orgs"][exporter]
    org_job = TargzInstallJob(wd=org_distros_dir, url=org_url)
    org_res = TargzInstallFun.do(org_job)

    
    # install exporter
    exporter_repo = catalog["exporters"][exporter]
    exporter_job = GitInstallJob(wd=exporters_dir, repo=exporter_repo)
    exporter_res = GitInstallFun.do(exporter_job)
    exporter_dir = os.path.join(exporters_dir, exporter)

    # install theme
    theme_repo = catalog["themes"][theme][exporter]
    theme_job = GitInstallJob(wd=exporter_dir, repo=theme_repo)
    theme_res = GitInstallFun.do(theme_job)
    theme_dir = os.path.join(exporter_dir, theme)


    # install git directories under exporter/git
    git_dir = os.path.join(exporter_dir, "git")
    os.system('mkdir -p %s' % git_dir)

    gits = catalog["gits"][exporter]
    for repo in gits:
        job = GitInstallJob(wd=git_dir, repo=repo['url'])
        res = GitInstallFun.do(job)

    # Create symlink "exp" under prj_dir to point to exporter
    exp_symlink_job = SymLinkJob(wd=prj_dir,
                                 dest=os.path.realpath(os.path.abspath(exporter_dir)),\
                                 link="exp")
    exp_symlink_res = SymLinkFun.do(exp_symlink_job)

    
    # symlink exporter_dir/org-distro ->
    # ext/org-distros/org-v.j.n
    
    org_symlink_job = SymLinkJob(wd=exporter_dir,
                                 dest=os.path.abspath(org_res.ws_dir),\
                                 link="org-distro")
    
    org_symlink_res = SymLinkFun.do(org_symlink_job)
    
    # symlink org-templates
    # prj_dir/src/org-templates -> ext/exporters/<exporter>/<theme>/org-templates
    print "theme_res.ws_dir = %s" % theme_res.ws_dir
    # cd src_dir; ln -s theme/org-templates
    org_templates_job = SymLinkJob(wd=src_dir, \
                                   dest=os.path.abspath(os.path.join(theme_res.ws_dir,\
                                                                     'org-templates')))
    org_templates_res = SymLinkFun.do(org_templates_job)
    
    
    # symlink style
    
    style_job = SymLinkJob(wd=src_dir, \
                           dest=os.path.abspath(os.path.join(theme_res.ws_dir,\
                                             'style')))
    style_res = SymLinkFun.do(style_job)


    if (math_theme != None):
        print "installing math theme %s" % math_theme
        # install math theme
        math_repo = catalog["math_themes"][math_theme]
        math_job = GitInstallJob(wd=ext, repo=math_repo)
        math_res = GitInstallFun.do(math_job)
        # math theme installed in ws ${ext}/<math-theme>
        math_dir = os.path.join(ext, math_theme)

        # symlink math 
        math_symlink_job = SymLinkJob(wd=src_dir, \
                                      dest=os.path.abspath(os.path.join(math_res.ws_dir,\
                                                                        'org-templates')),
                                      link="math-templates")
        
        math_symlink_res = SymLinkFun.do(math_symlink_job)

        # symlink math-style
        math_style_job = SymLinkJob(wd=src_dir, \
                                    dest=os.path.abspath(os.path.join(math_res.ws_dir,\
                                                                      'style')),
                                    link="math-style")
        
        math_style_res = SymLinkFun.do(math_style_job)
    else:
        print "no math theme to install"


if __name__ == "__main__":o
    main(sys.argv[1:])




    

 
 
 
