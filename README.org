#+title:  README


* Release Notes for v2.0

** User Interface
Publisher is invoked by the make targets:

 - init: installs make-utils
 - configure:  configures the publisher
 - publish: does the publishing task.

The makefile assumes a =config_file= at =../config.py=. 

** Dependencies

 - [[https://github.com/vlead/py2_utils.git][py2-utils]] :: v0.2.0 on branch develop
 - [[https://github.com/vlead/make-utils.git][make-utils]] :: master branch 

